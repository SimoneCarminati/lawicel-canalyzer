﻿namespace LAWICEL_CANalyzer
{
    partial class Form_CreateScript
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_CreateScript));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tb_scriptName = new System.Windows.Forms.ToolStripTextBox();
            this.btn_SaveScript = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btn_AddMessage = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteMessage = new System.Windows.Forms.ToolStripButton();
            this.dgv_CANTrace = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RTR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANTrace)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tb_scriptName,
            this.btn_SaveScript});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(712, 50);
            this.toolStrip1.TabIndex = 10;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(72, 47);
            this.toolStripLabel1.Text = "Script Name";
            // 
            // tb_scriptName
            // 
            this.tb_scriptName.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_scriptName.Name = "tb_scriptName";
            this.tb_scriptName.Size = new System.Drawing.Size(200, 50);
            this.tb_scriptName.Text = "Script Name";
            this.tb_scriptName.Leave += new System.EventHandler(this.tb_scriptName_Leave);
            this.tb_scriptName.Click += new System.EventHandler(this.tb_scriptName_Click);
            // 
            // btn_SaveScript
            // 
            this.btn_SaveScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_SaveScript.Image = global::LAWICEL_CANalyzer.Properties.Resources.save;
            this.btn_SaveScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_SaveScript.Name = "btn_SaveScript";
            this.btn_SaveScript.Size = new System.Drawing.Size(51, 47);
            this.btn_SaveScript.Text = "Save Script";
            this.btn_SaveScript.Click += new System.EventHandler(this.btn_SaveScript_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_AddMessage,
            this.btn_DeleteMessage});
            this.toolStrip2.Location = new System.Drawing.Point(0, 50);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(712, 50);
            this.toolStrip2.TabIndex = 11;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btn_AddMessage
            // 
            this.btn_AddMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddMessage.Image = global::LAWICEL_CANalyzer.Properties.Resources.add;
            this.btn_AddMessage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddMessage.Name = "btn_AddMessage";
            this.btn_AddMessage.Size = new System.Drawing.Size(51, 47);
            this.btn_AddMessage.Text = "Add Message";
            this.btn_AddMessage.Click += new System.EventHandler(this.btn_AddMessage_Click);
            // 
            // btn_DeleteMessage
            // 
            this.btn_DeleteMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteMessage.Image = global::LAWICEL_CANalyzer.Properties.Resources.delete;
            this.btn_DeleteMessage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteMessage.Name = "btn_DeleteMessage";
            this.btn_DeleteMessage.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteMessage.Text = "Remove Message";
            this.btn_DeleteMessage.Click += new System.EventHandler(this.btn_DeleteMessage_Click);
            // 
            // dgv_CANTrace
            // 
            this.dgv_CANTrace.AllowUserToAddRows = false;
            this.dgv_CANTrace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CANTrace.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.DLC,
            this.RTR,
            this.D0,
            this.D1,
            this.D2,
            this.D3,
            this.D4,
            this.D5,
            this.D6,
            this.D7,
            this.Timestamp});
            this.dgv_CANTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_CANTrace.Location = new System.Drawing.Point(0, 100);
            this.dgv_CANTrace.Name = "dgv_CANTrace";
            this.dgv_CANTrace.Size = new System.Drawing.Size(712, 537);
            this.dgv_CANTrace.TabIndex = 12;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Width = 50;
            // 
            // DLC
            // 
            this.DLC.DataPropertyName = "DLC";
            this.DLC.HeaderText = "DLC";
            this.DLC.Name = "DLC";
            this.DLC.Width = 50;
            // 
            // RTR
            // 
            this.RTR.DataPropertyName = "RTR";
            this.RTR.HeaderText = "RTR";
            this.RTR.Name = "RTR";
            this.RTR.Width = 50;
            // 
            // D0
            // 
            this.D0.DataPropertyName = "D0";
            this.D0.HeaderText = "D0";
            this.D0.Name = "D0";
            this.D0.Width = 50;
            // 
            // D1
            // 
            this.D1.DataPropertyName = "D1";
            this.D1.HeaderText = "D1";
            this.D1.Name = "D1";
            this.D1.Width = 50;
            // 
            // D2
            // 
            this.D2.DataPropertyName = "D2";
            this.D2.HeaderText = "D2";
            this.D2.Name = "D2";
            this.D2.Width = 50;
            // 
            // D3
            // 
            this.D3.DataPropertyName = "D3";
            this.D3.HeaderText = "D3";
            this.D3.Name = "D3";
            this.D3.Width = 50;
            // 
            // D4
            // 
            this.D4.DataPropertyName = "D4";
            this.D4.HeaderText = "D4";
            this.D4.Name = "D4";
            this.D4.Width = 50;
            // 
            // D5
            // 
            this.D5.DataPropertyName = "D5";
            this.D5.HeaderText = "D5";
            this.D5.Name = "D5";
            this.D5.ReadOnly = true;
            this.D5.Width = 50;
            // 
            // D6
            // 
            this.D6.DataPropertyName = "D6";
            this.D6.HeaderText = "D6";
            this.D6.Name = "D6";
            this.D6.Width = 50;
            // 
            // D7
            // 
            this.D7.DataPropertyName = "D7";
            this.D7.HeaderText = "D7";
            this.D7.Name = "D7";
            this.D7.Width = 50;
            // 
            // Timestamp
            // 
            this.Timestamp.DataPropertyName = "Timestamp";
            this.Timestamp.HeaderText = "Timestamp";
            this.Timestamp.Name = "Timestamp";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            // 
            // Form_CreateScript
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 637);
            this.Controls.Add(this.dgv_CANTrace);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_CreateScript";
            this.Text = "Create Script";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANTrace)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tb_scriptName;
        private System.Windows.Forms.ToolStripButton btn_SaveScript;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btn_AddMessage;
        private System.Windows.Forms.ToolStripButton btn_DeleteMessage;
        private System.Windows.Forms.DataGridView dgv_CANTrace;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTR;
        private System.Windows.Forms.DataGridViewTextBoxColumn D0;
        private System.Windows.Forms.DataGridViewTextBoxColumn D1;
        private System.Windows.Forms.DataGridViewTextBoxColumn D2;
        private System.Windows.Forms.DataGridViewTextBoxColumn D3;
        private System.Windows.Forms.DataGridViewTextBoxColumn D4;
        private System.Windows.Forms.DataGridViewTextBoxColumn D5;
        private System.Windows.Forms.DataGridViewTextBoxColumn D6;
        private System.Windows.Forms.DataGridViewTextBoxColumn D7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Timestamp;
        private System.Windows.Forms.ImageList imageList;
    }
}