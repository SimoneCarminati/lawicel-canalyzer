﻿namespace LAWICEL_CANalyzer
{
    partial class Form_AddMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_AddMessage));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_Timestamp = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_ID = new System.Windows.Forms.MaskedTextBox();
            this.cb_RTR = new System.Windows.Forms.CheckBox();
            this.tb_D7 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D6 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D5 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D4 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D3 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D2 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D1 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D0 = new System.Windows.Forms.MaskedTextBox();
            this.tb_DLC = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Submit = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_Timestamp);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_ID);
            this.groupBox1.Controls.Add(this.cb_RTR);
            this.groupBox1.Controls.Add(this.tb_D7);
            this.groupBox1.Controls.Add(this.tb_D6);
            this.groupBox1.Controls.Add(this.tb_D5);
            this.groupBox1.Controls.Add(this.tb_D4);
            this.groupBox1.Controls.Add(this.tb_D3);
            this.groupBox1.Controls.Add(this.tb_D2);
            this.groupBox1.Controls.Add(this.tb_D1);
            this.groupBox1.Controls.Add(this.tb_D0);
            this.groupBox1.Controls.Add(this.tb_DLC);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(610, 87);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CAN Message";
            // 
            // tb_Timestamp
            // 
            this.tb_Timestamp.Location = new System.Drawing.Point(518, 42);
            this.tb_Timestamp.Mask = "9999999";
            this.tb_Timestamp.Name = "tb_Timestamp";
            this.tb_Timestamp.Size = new System.Drawing.Size(53, 20);
            this.tb_Timestamp.TabIndex = 12;
            this.tb_Timestamp.Enter += new System.EventHandler(this.tb_Timestamp_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(503, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Timestamp (ms)";
            // 
            // tb_ID
            // 
            this.tb_ID.Location = new System.Drawing.Point(19, 42);
            this.tb_ID.Mask = "&&&";
            this.tb_ID.Name = "tb_ID";
            this.tb_ID.Size = new System.Drawing.Size(28, 20);
            this.tb_ID.TabIndex = 1;
            // 
            // cb_RTR
            // 
            this.cb_RTR.AutoSize = true;
            this.cb_RTR.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cb_RTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_RTR.Location = new System.Drawing.Point(109, 25);
            this.cb_RTR.Name = "cb_RTR";
            this.cb_RTR.Size = new System.Drawing.Size(37, 31);
            this.cb_RTR.TabIndex = 3;
            this.cb_RTR.Text = "RTR\r\n";
            this.cb_RTR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb_RTR.UseVisualStyleBackColor = true;
            // 
            // tb_D7
            // 
            this.tb_D7.Location = new System.Drawing.Point(476, 42);
            this.tb_D7.Mask = "&&";
            this.tb_D7.Name = "tb_D7";
            this.tb_D7.Size = new System.Drawing.Size(20, 20);
            this.tb_D7.TabIndex = 11;
            // 
            // tb_D6
            // 
            this.tb_D6.Location = new System.Drawing.Point(434, 42);
            this.tb_D6.Mask = "&&";
            this.tb_D6.Name = "tb_D6";
            this.tb_D6.Size = new System.Drawing.Size(20, 20);
            this.tb_D6.TabIndex = 10;
            // 
            // tb_D5
            // 
            this.tb_D5.Location = new System.Drawing.Point(386, 42);
            this.tb_D5.Mask = "&&";
            this.tb_D5.Name = "tb_D5";
            this.tb_D5.Size = new System.Drawing.Size(20, 20);
            this.tb_D5.TabIndex = 9;
            // 
            // tb_D4
            // 
            this.tb_D4.Location = new System.Drawing.Point(341, 42);
            this.tb_D4.Mask = "&&";
            this.tb_D4.Name = "tb_D4";
            this.tb_D4.Size = new System.Drawing.Size(20, 20);
            this.tb_D4.TabIndex = 8;
            // 
            // tb_D3
            // 
            this.tb_D3.Location = new System.Drawing.Point(296, 42);
            this.tb_D3.Mask = "&&";
            this.tb_D3.Name = "tb_D3";
            this.tb_D3.Size = new System.Drawing.Size(20, 20);
            this.tb_D3.TabIndex = 7;
            // 
            // tb_D2
            // 
            this.tb_D2.Location = new System.Drawing.Point(251, 42);
            this.tb_D2.Mask = "&&";
            this.tb_D2.Name = "tb_D2";
            this.tb_D2.Size = new System.Drawing.Size(20, 20);
            this.tb_D2.TabIndex = 6;
            // 
            // tb_D1
            // 
            this.tb_D1.Location = new System.Drawing.Point(206, 42);
            this.tb_D1.Mask = "&&";
            this.tb_D1.Name = "tb_D1";
            this.tb_D1.Size = new System.Drawing.Size(20, 20);
            this.tb_D1.TabIndex = 5;
            // 
            // tb_D0
            // 
            this.tb_D0.Location = new System.Drawing.Point(161, 42);
            this.tb_D0.Mask = "&&";
            this.tb_D0.Name = "tb_D0";
            this.tb_D0.Size = new System.Drawing.Size(20, 20);
            this.tb_D0.TabIndex = 4;
            // 
            // tb_DLC
            // 
            this.tb_DLC.Location = new System.Drawing.Point(72, 42);
            this.tb_DLC.Mask = "0";
            this.tb_DLC.Name = "tb_DLC";
            this.tb_DLC.Size = new System.Drawing.Size(20, 20);
            this.tb_DLC.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(473, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "D7";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(431, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "D6";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(383, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "D5";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(338, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "D4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(294, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "D3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(249, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "D2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(204, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "D1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(159, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "D0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(66, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "DLC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // btn_Submit
            // 
            this.btn_Submit.ImageKey = "ok.png";
            this.btn_Submit.ImageList = this.imageList;
            this.btn_Submit.Location = new System.Drawing.Point(229, 117);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(50, 50);
            this.btn_Submit.TabIndex = 13;
            this.btn_Submit.UseVisualStyleBackColor = true;
            this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.ImageKey = "cancel.png";
            this.btn_Cancel.ImageList = this.imageList;
            this.btn_Cancel.Location = new System.Drawing.Point(337, 117);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_Cancel.TabIndex = 14;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // Form_AddMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 179);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Submit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_AddMessage";
            this.Text = "Add Message";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox tb_ID;
        private System.Windows.Forms.CheckBox cb_RTR;
        private System.Windows.Forms.MaskedTextBox tb_D7;
        private System.Windows.Forms.MaskedTextBox tb_D6;
        private System.Windows.Forms.MaskedTextBox tb_D5;
        private System.Windows.Forms.MaskedTextBox tb_D4;
        private System.Windows.Forms.MaskedTextBox tb_D3;
        private System.Windows.Forms.MaskedTextBox tb_D2;
        private System.Windows.Forms.MaskedTextBox tb_D1;
        private System.Windows.Forms.MaskedTextBox tb_D0;
        private System.Windows.Forms.MaskedTextBox tb_DLC;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox tb_Timestamp;
        private System.Windows.Forms.ImageList imageList;
    }
}