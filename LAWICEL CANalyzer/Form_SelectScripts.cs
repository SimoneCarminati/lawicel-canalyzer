﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace LAWICEL_CANalyzer
{
    public partial class Form_SelectScripts : Form
    {
        public List<String> scripts;
        public Form_SelectScripts()
        {
            InitializeComponent();
            initializeDataGridViews(Environment.CurrentDirectory + @"\Scripts");
        }

        private void initializeDataGridViews(string path)
        {
            if (!Directory.GetFiles(path).Count().Equals(0))
                foreach (String file in Directory.GetFiles(path))
                    dgv_scriptsNotSelected.Rows.Insert(0, file.Replace(Environment.CurrentDirectory + @"\Scripts\",""));
            if (!Directory.GetDirectories(path).Count().Equals(0))
                foreach (String dir in Directory.GetDirectories(path))
                    initializeDataGridViews(dir);
        }

        private void btn_Select_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_scriptsSelected.Rows.Add(dgv_scriptsNotSelected.CurrentRow.Cells[0].Value);
                dgv_scriptsNotSelected.Rows.Remove(dgv_scriptsNotSelected.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Deselect_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_scriptsNotSelected.Rows.Add(dgv_scriptsSelected.CurrentRow.Cells[0].Value);
                dgv_scriptsSelected.Rows.Remove(dgv_scriptsSelected.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                scripts = new List<String>();
                foreach (DataGridViewRow r in dgv_scriptsSelected.Rows)
                    scripts.Add(r.Cells[0].Value.ToString());
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
