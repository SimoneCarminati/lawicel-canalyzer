﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace LAWICEL_CANalyzer
{
    public partial class FormMain : Form
    {
        private ToolTip tooltip = new System.Windows.Forms.ToolTip();
        private CANusb_Connector connector;
        private List<List<CANCommand>> cmdList;
        private List<ThreadTestVita> threadsTestVita;
        delegate void CANCommandArgReturningVoidDelegate(DataGridView dgv, CANCommand msg);
        public FormMain()
        {
            InitializeComponent();
            setActiveDongles();
            setToolTips();
        }

        private void setActiveDongles()
        {
            try
            {
                List<String> dongleList = new List<string>();
                dongleList = Utility.getCanUSBList();
                cmb_InterfaceList.Items.Clear();
                if (dongleList != null && !dongleList.Count.Equals(0))
                {
                    foreach (string d in dongleList)
                        cmb_InterfaceList.Items.Add(d);
                    cmb_InterfaceList.BackColor = Color.White;
                    cmb_InterfaceList.SelectedIndex = 0;
                }
                else
                {
                    cmb_InterfaceList.BackColor = Color.Salmon;
                    cmb_InterfaceList.SelectedIndex = -1;
                    cmb_InterfaceList.Text = "";
                    cmb_InterfaceList.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            setActiveDongles();
        }

        private void setToolTips()
        {
            tooltip.SetToolTip(btn_Submit, "Submin Message");
            tooltip.SetToolTip(btn_createScript, "Create Script");
            tooltip.SetToolTip(btn_selectScripts, "Select Scripts");
            tooltip.SetToolTip(btn_startScripts, "Start Scripts");
        }

        private void btn_OnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_OnOff.Tag.Equals("On"))
                {
                    if (cmb_InterfaceList.SelectedItem == null)
                        MessageBox.Show("Select a valid CANUSB Device", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (cmb_BaudRate.SelectedItem == null)
                        MessageBox.Show("Select a valid Baud Rate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        connector = new CANusb_Connector(cmb_InterfaceList.SelectedItem.ToString(), cmb_BaudRate.SelectedItem.ToString());
                        if (connector.IsConnected)
                        {
                            //Connessione OK
                            connector.Handler += ConnectorOnHandler();
                            btn_OnOff.Tag = "Off";
                            cmb_InterfaceList.Enabled = false;
                            cmb_BaudRate.Enabled = false;
                            btn_OnOff.Image = global::LAWICEL_CANalyzer.Properties.Resources.offIcon;
                            cmdList = new List<List<CANCommand>>();
                            grp_SendMessage.Enabled = true;
                            grp_Scripts.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Connection to the device failed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            setActiveDongles();
                        }
                    }
                }
                else if (btn_OnOff.Tag.Equals("Off"))
                {
                    connector.CANusb_disconnect();
                    if (!connector.IsConnected)
                    {
                        //Disconnessione OK
                        btn_OnOff.Tag = "On";
                        cmb_InterfaceList.Enabled = true;
                        cmb_BaudRate.Enabled = true;
                        btn_OnOff.Image = global::LAWICEL_CANalyzer.Properties.Resources.onIcon;
                        grp_SendMessage.Enabled = false;
                        grp_Scripts.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Disconnection from the device failed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        setActiveDongles();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Clean_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_CANTrace.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Filter_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Filter())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        form.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate (CANCommand value) { this.EmitMessageChange(value); };
        }

        private void EmitMessageChange(CANCommand message)
        {
            InsertIntoDataGridView(dgv_CANTrace, message);
        }

        private void InsertIntoDataGridView(DataGridView dgv, CANCommand msg)
        {
            // InvokeRequired required compares the thread ID of the  
            // calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.
            if (dgv.InvokeRequired)
            {
                CANCommandArgReturningVoidDelegate d = new CANCommandArgReturningVoidDelegate(InsertIntoDataGridView);
                this.Invoke(d, new object[] { dgv, msg });
            }
            else
            {
                //per migliorare le prestazioni, faccio visualizzare max 200 righe
                if (dgv.Rows.Count.Equals(200))
                    dgv.Rows.RemoveAt(dgv.Rows.Count-1);
                dgv.Rows.Insert(0, msg.ActDateTime.ToString("HH:mm:ss:ffff"), msg.MessageType == 0?"Rx":"Tx", msg.ID.ToString("X3"), msg.DLC, msg.Flags, msg.DataBytes.d0.ToString("X2"), msg.DataBytes.d1.ToString("X2"), msg.DataBytes.d2.ToString("X2"), msg.DataBytes.d3.ToString("X2"), msg.DataBytes.d4.ToString("X2"), msg.DataBytes.d5.ToString("X2"), msg.DataBytes.d6.ToString("X2"), msg.DataBytes.d7.ToString("X2"), msg.TimeStamp);
            }
        }

        #region SendMessage

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            CANCommand msg = new CANCommand(Convert.ToUInt32(Utility.hexValidator(tb_ID.Text, 3), 16), Convert.ToByte(Utility.hexValidator(tb_DLC.Text, 1), 16), cb_RTR.Checked ? (byte)0x01 : (byte)0x00, Convert.ToByte(Utility.hexValidator(tb_D0.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D1.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D2.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D3.Text, 3), 16), Convert.ToByte(Utility.hexValidator(tb_D4.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D5.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D6.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D7.Text, 2), 16));
            connector.enqueueMessage(msg);
        }

        #endregion

        #region Scripts

        private void btn_createScript_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_CreateScript())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        form.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_selectScripts_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_SelectScripts())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        cmdList = Utility.loadScripts(form.scripts);
                    }
                    form.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_startScripts_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmdList != null && !cmdList.Count.Equals(0))
                {
                    if (btn_startScripts.Tag.Equals("Start"))
                    {
                        tooltip.SetToolTip(btn_startScripts, "Stop Scripts");
                        btn_startScripts.Tag = "Stop";
                        btn_startScripts.ImageIndex = 3;
                        threadsTestVita = creaTestVita(cmdList);
                        foreach (ThreadTestVita item in threadsTestVita)
                        {
                            item.thread.Start();
                        }
                    }
                    else if (btn_startScripts.Tag.Equals("Stop"))
                    {
                        tooltip.SetToolTip(btn_startScripts, "Start Scripts");
                        btn_startScripts.Tag = "Start";
                        btn_startScripts.ImageIndex = 2;
                        foreach (ThreadTestVita item in threadsTestVita)
                        {
                            item.stopThread();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Select a valid script file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<ThreadTestVita> creaTestVita(List<List<CANCommand>> cmdList)
        {
            ThreadTestVita threadTestVita;
            List<ThreadTestVita> lista = new List<ThreadTestVita>();

            int scriptNumber = 0;
            foreach (List<CANCommand> list in cmdList)
            {
                threadTestVita = new ThreadTestVita(connector, list, scriptNumber);
                threadTestVita.thread = new Thread(new ThreadStart(threadTestVita.startThread));
                threadTestVita.thread.IsBackground = true;

                lista.Add(threadTestVita);
                scriptNumber++;
            }
            return lista;
        }

        #endregion

        #region focusMaskedtextBox

        private void tb_ID_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_ID.SelectAll();
        }

        private void tb_DLC_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_DLC.SelectAll();
        }

        private void tb_D0_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D0.SelectAll();
        }

        private void tb_D1_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D1.SelectAll();
        }

        private void tb_D2_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D2.SelectAll();
        }

        private void tb_D3_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D3.SelectAll();
        }

        private void tb_D4_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D4.SelectAll();
        }

        private void tb_D5_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D5.SelectAll();
        }

        private void tb_D6_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D6.SelectAll();
        }

        private void tb_D7_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D7.SelectAll();
        }

        private void SetMaskedTextBoxSelectAll(MaskedTextBox txtbox)
        {
            txtbox.SelectAll();
        }

        #endregion

    }
}
