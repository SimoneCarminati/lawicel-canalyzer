﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAWICEL_CANalyzer
{
    public enum ProtocolChange
    {
        Wait,
        FileTransferStart,
        BlockTransferStart,
        BlockTransferComplete,
        DownloadComplete,
        FileTransferComplete
    }

    interface IProtocolDataChanges<out ProtocolChange>
    {
        event Action<ProtocolChange> Handler;
    }
}
