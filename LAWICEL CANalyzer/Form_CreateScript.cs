﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAWICEL_CANalyzer
{
    public partial class Form_CreateScript : Form
    {
        public Form_CreateScript()
        {
            InitializeComponent();
        }

        private void btn_AddMessage_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_AddMessage())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        dgv_CANTrace.Rows.Insert(0, form.msg.ID.ToString("X3"), form.msg.DLC, form.msg.Flags, form.msg.DataBytes.d0.ToString("X2"), form.msg.DataBytes.d1.ToString("X2"), form.msg.DataBytes.d2.ToString("X2"), form.msg.DataBytes.d3.ToString("X2"), form.msg.DataBytes.d4.ToString("X2"), form.msg.DataBytes.d5.ToString("X2"), form.msg.DataBytes.d6.ToString("X2"), form.msg.DataBytes.d7.ToString("X2"), form.msg.TimeStamp);
                    }
                    form.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteMessage_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_CANTrace.Rows.Remove(dgv_CANTrace.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_SaveScript_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.saveScript(dgv_CANTrace, tb_scriptName.Text))
                {
                    MessageBox.Show("Script file saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                }
                else
                    MessageBox.Show("Error while saving script file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_scriptName_Click(object sender, EventArgs e)
        {
            tb_scriptName.Text = "";
            tb_scriptName.ForeColor = Color.Black;
        }

        private void tb_scriptName_Leave(object sender, EventArgs e)
        {
            if (tb_scriptName.Text.Equals(""))
            {
                tb_scriptName.ForeColor = SystemColors.ControlDark;
                tb_scriptName.Text = "Script Name";
            }
            else
                tb_scriptName.ForeColor = Color.Black;
        }
    }
}
