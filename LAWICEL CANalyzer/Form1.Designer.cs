﻿namespace LAWICEL_CANalyzer
{
    partial class FormMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panel_CANUSB_Statistics = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel_CANTrace = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgv_CANTrace = new System.Windows.Forms.DataGridView();
            this.dateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RTR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grp_Scripts = new System.Windows.Forms.GroupBox();
            this.btn_startScripts = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btn_selectScripts = new System.Windows.Forms.Button();
            this.btn_createScript = new System.Windows.Forms.Button();
            this.grp_SendMessage = new System.Windows.Forms.GroupBox();
            this.tb_ID = new System.Windows.Forms.MaskedTextBox();
            this.cb_RTR = new System.Windows.Forms.CheckBox();
            this.btn_Submit = new System.Windows.Forms.Button();
            this.tb_D7 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D6 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D5 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D4 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D3 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D2 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D1 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D0 = new System.Windows.Forms.MaskedTextBox();
            this.tb_DLC = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_Refresh = new System.Windows.Forms.ToolStripButton();
            this.cmb_BaudRate = new System.Windows.Forms.ToolStripComboBox();
            this.cmb_InterfaceList = new System.Windows.Forms.ToolStripComboBox();
            this.btn_OnOff = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_Clean = new System.Windows.Forms.ToolStripButton();
            this.btn_Filter = new System.Windows.Forms.ToolStripButton();
            this.panel_CANUSB_Statistics.SuspendLayout();
            this.panel_CANTrace.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANTrace)).BeginInit();
            this.panel2.SuspendLayout();
            this.grp_Scripts.SuspendLayout();
            this.grp_SendMessage.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_CANUSB_Statistics
            // 
            this.panel_CANUSB_Statistics.Controls.Add(this.groupBox2);
            this.panel_CANUSB_Statistics.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_CANUSB_Statistics.Location = new System.Drawing.Point(0, 0);
            this.panel_CANUSB_Statistics.Name = "panel_CANUSB_Statistics";
            this.panel_CANUSB_Statistics.Size = new System.Drawing.Size(143, 689);
            this.panel_CANUSB_Statistics.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(6, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(131, 679);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Statistics";
            // 
            // panel_CANTrace
            // 
            this.panel_CANTrace.Controls.Add(this.panel1);
            this.panel_CANTrace.Controls.Add(this.toolStrip1);
            this.panel_CANTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_CANTrace.Location = new System.Drawing.Point(143, 0);
            this.panel_CANTrace.Name = "panel_CANTrace";
            this.panel_CANTrace.Size = new System.Drawing.Size(870, 689);
            this.panel_CANTrace.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgv_CANTrace);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 639);
            this.panel1.TabIndex = 9;
            // 
            // dgv_CANTrace
            // 
            this.dgv_CANTrace.AllowUserToAddRows = false;
            this.dgv_CANTrace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CANTrace.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateTime,
            this.dataGridViewTextBoxColumn1,
            this.ID,
            this.DLC,
            this.RTR,
            this.D0,
            this.D1,
            this.D2,
            this.D3,
            this.D4,
            this.D5,
            this.D6,
            this.D7,
            this.Timestamp});
            this.dgv_CANTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_CANTrace.Location = new System.Drawing.Point(0, 0);
            this.dgv_CANTrace.Name = "dgv_CANTrace";
            this.dgv_CANTrace.ReadOnly = true;
            this.dgv_CANTrace.Size = new System.Drawing.Size(870, 539);
            this.dgv_CANTrace.TabIndex = 1;
            // 
            // dateTime
            // 
            this.dateTime.HeaderText = "DateTime";
            this.dateTime.Name = "dateTime";
            this.dateTime.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TxRx";
            this.dataGridViewTextBoxColumn1.HeaderText = "TxRx";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 50;
            // 
            // DLC
            // 
            this.DLC.DataPropertyName = "DLC";
            this.DLC.HeaderText = "DLC";
            this.DLC.Name = "DLC";
            this.DLC.ReadOnly = true;
            this.DLC.Width = 50;
            // 
            // RTR
            // 
            this.RTR.DataPropertyName = "RTR";
            this.RTR.HeaderText = "RTR";
            this.RTR.Name = "RTR";
            this.RTR.ReadOnly = true;
            this.RTR.Width = 50;
            // 
            // D0
            // 
            this.D0.DataPropertyName = "D0";
            this.D0.HeaderText = "D0";
            this.D0.Name = "D0";
            this.D0.ReadOnly = true;
            this.D0.Width = 50;
            // 
            // D1
            // 
            this.D1.DataPropertyName = "D1";
            this.D1.HeaderText = "D1";
            this.D1.Name = "D1";
            this.D1.ReadOnly = true;
            this.D1.Width = 50;
            // 
            // D2
            // 
            this.D2.DataPropertyName = "D2";
            this.D2.HeaderText = "D2";
            this.D2.Name = "D2";
            this.D2.ReadOnly = true;
            this.D2.Width = 50;
            // 
            // D3
            // 
            this.D3.DataPropertyName = "D3";
            this.D3.HeaderText = "D3";
            this.D3.Name = "D3";
            this.D3.ReadOnly = true;
            this.D3.Width = 50;
            // 
            // D4
            // 
            this.D4.DataPropertyName = "D4";
            this.D4.HeaderText = "D4";
            this.D4.Name = "D4";
            this.D4.ReadOnly = true;
            this.D4.Width = 50;
            // 
            // D5
            // 
            this.D5.DataPropertyName = "D5";
            this.D5.HeaderText = "D5";
            this.D5.Name = "D5";
            this.D5.ReadOnly = true;
            this.D5.Width = 50;
            // 
            // D6
            // 
            this.D6.DataPropertyName = "D6";
            this.D6.HeaderText = "D6";
            this.D6.Name = "D6";
            this.D6.ReadOnly = true;
            this.D6.Width = 50;
            // 
            // D7
            // 
            this.D7.DataPropertyName = "D7";
            this.D7.HeaderText = "D7";
            this.D7.Name = "D7";
            this.D7.ReadOnly = true;
            this.D7.Width = 50;
            // 
            // Timestamp
            // 
            this.Timestamp.DataPropertyName = "Timestamp";
            this.Timestamp.HeaderText = "Timestamp";
            this.Timestamp.Name = "Timestamp";
            this.Timestamp.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.grp_Scripts);
            this.panel2.Controls.Add(this.grp_SendMessage);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 539);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 100);
            this.panel2.TabIndex = 0;
            // 
            // grp_Scripts
            // 
            this.grp_Scripts.Controls.Add(this.btn_startScripts);
            this.grp_Scripts.Controls.Add(this.btn_selectScripts);
            this.grp_Scripts.Controls.Add(this.btn_createScript);
            this.grp_Scripts.Enabled = false;
            this.grp_Scripts.Location = new System.Drawing.Point(602, 6);
            this.grp_Scripts.Name = "grp_Scripts";
            this.grp_Scripts.Size = new System.Drawing.Size(256, 87);
            this.grp_Scripts.TabIndex = 1;
            this.grp_Scripts.TabStop = false;
            this.grp_Scripts.Text = "Scripts";
            // 
            // btn_startScripts
            // 
            this.btn_startScripts.ImageKey = "start.png";
            this.btn_startScripts.ImageList = this.imageList;
            this.btn_startScripts.Location = new System.Drawing.Point(186, 20);
            this.btn_startScripts.Name = "btn_startScripts";
            this.btn_startScripts.Size = new System.Drawing.Size(50, 50);
            this.btn_startScripts.TabIndex = 16;
            this.btn_startScripts.Tag = "Start";
            this.btn_startScripts.UseVisualStyleBackColor = true;
            this.btn_startScripts.Click += new System.EventHandler(this.btn_startScripts_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            this.imageList.Images.SetKeyName(10, "rubber.png");
            this.imageList.Images.SetKeyName(11, "filter.png");
            // 
            // btn_selectScripts
            // 
            this.btn_selectScripts.ImageKey = "openFiles.png";
            this.btn_selectScripts.ImageList = this.imageList;
            this.btn_selectScripts.Location = new System.Drawing.Point(104, 20);
            this.btn_selectScripts.Name = "btn_selectScripts";
            this.btn_selectScripts.Size = new System.Drawing.Size(50, 50);
            this.btn_selectScripts.TabIndex = 15;
            this.btn_selectScripts.UseVisualStyleBackColor = true;
            this.btn_selectScripts.Click += new System.EventHandler(this.btn_selectScripts_Click);
            // 
            // btn_createScript
            // 
            this.btn_createScript.ImageKey = "addScript.png";
            this.btn_createScript.ImageList = this.imageList;
            this.btn_createScript.Location = new System.Drawing.Point(22, 20);
            this.btn_createScript.Name = "btn_createScript";
            this.btn_createScript.Size = new System.Drawing.Size(50, 50);
            this.btn_createScript.TabIndex = 14;
            this.btn_createScript.UseVisualStyleBackColor = true;
            this.btn_createScript.Click += new System.EventHandler(this.btn_createScript_Click);
            // 
            // grp_SendMessage
            // 
            this.grp_SendMessage.Controls.Add(this.tb_ID);
            this.grp_SendMessage.Controls.Add(this.cb_RTR);
            this.grp_SendMessage.Controls.Add(this.btn_Submit);
            this.grp_SendMessage.Controls.Add(this.tb_D7);
            this.grp_SendMessage.Controls.Add(this.tb_D6);
            this.grp_SendMessage.Controls.Add(this.tb_D5);
            this.grp_SendMessage.Controls.Add(this.tb_D4);
            this.grp_SendMessage.Controls.Add(this.tb_D3);
            this.grp_SendMessage.Controls.Add(this.tb_D2);
            this.grp_SendMessage.Controls.Add(this.tb_D1);
            this.grp_SendMessage.Controls.Add(this.tb_D0);
            this.grp_SendMessage.Controls.Add(this.tb_DLC);
            this.grp_SendMessage.Controls.Add(this.label11);
            this.grp_SendMessage.Controls.Add(this.label10);
            this.grp_SendMessage.Controls.Add(this.label9);
            this.grp_SendMessage.Controls.Add(this.label8);
            this.grp_SendMessage.Controls.Add(this.label7);
            this.grp_SendMessage.Controls.Add(this.label6);
            this.grp_SendMessage.Controls.Add(this.label5);
            this.grp_SendMessage.Controls.Add(this.label4);
            this.grp_SendMessage.Controls.Add(this.label2);
            this.grp_SendMessage.Controls.Add(this.label1);
            this.grp_SendMessage.Enabled = false;
            this.grp_SendMessage.Location = new System.Drawing.Point(6, 6);
            this.grp_SendMessage.Name = "grp_SendMessage";
            this.grp_SendMessage.Size = new System.Drawing.Size(590, 87);
            this.grp_SendMessage.TabIndex = 0;
            this.grp_SendMessage.TabStop = false;
            this.grp_SendMessage.Text = "Send Message";
            // 
            // tb_ID
            // 
            this.tb_ID.Location = new System.Drawing.Point(23, 42);
            this.tb_ID.Mask = "&&&";
            this.tb_ID.Name = "tb_ID";
            this.tb_ID.Size = new System.Drawing.Size(28, 20);
            this.tb_ID.TabIndex = 2;
            this.tb_ID.Enter += new System.EventHandler(this.tb_ID_Enter);
            // 
            // cb_RTR
            // 
            this.cb_RTR.AutoSize = true;
            this.cb_RTR.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cb_RTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_RTR.Location = new System.Drawing.Point(113, 25);
            this.cb_RTR.Name = "cb_RTR";
            this.cb_RTR.Size = new System.Drawing.Size(37, 31);
            this.cb_RTR.TabIndex = 4;
            this.cb_RTR.Text = "RTR\r\n";
            this.cb_RTR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb_RTR.UseVisualStyleBackColor = true;
            // 
            // btn_Submit
            // 
            this.btn_Submit.ImageKey = "submit.png";
            this.btn_Submit.ImageList = this.imageList;
            this.btn_Submit.Location = new System.Drawing.Point(525, 20);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(50, 50);
            this.btn_Submit.TabIndex = 13;
            this.btn_Submit.UseVisualStyleBackColor = true;
            this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            // 
            // tb_D7
            // 
            this.tb_D7.Location = new System.Drawing.Point(480, 42);
            this.tb_D7.Mask = "&&";
            this.tb_D7.Name = "tb_D7";
            this.tb_D7.Size = new System.Drawing.Size(20, 20);
            this.tb_D7.TabIndex = 12;
            this.tb_D7.Enter += new System.EventHandler(this.tb_D7_Enter);
            // 
            // tb_D6
            // 
            this.tb_D6.Location = new System.Drawing.Point(438, 42);
            this.tb_D6.Mask = "&&";
            this.tb_D6.Name = "tb_D6";
            this.tb_D6.Size = new System.Drawing.Size(20, 20);
            this.tb_D6.TabIndex = 11;
            this.tb_D6.Enter += new System.EventHandler(this.tb_D6_Enter);
            // 
            // tb_D5
            // 
            this.tb_D5.Location = new System.Drawing.Point(390, 42);
            this.tb_D5.Mask = "&&";
            this.tb_D5.Name = "tb_D5";
            this.tb_D5.Size = new System.Drawing.Size(20, 20);
            this.tb_D5.TabIndex = 10;
            this.tb_D5.Enter += new System.EventHandler(this.tb_D5_Enter);
            // 
            // tb_D4
            // 
            this.tb_D4.Location = new System.Drawing.Point(345, 42);
            this.tb_D4.Mask = "&&";
            this.tb_D4.Name = "tb_D4";
            this.tb_D4.Size = new System.Drawing.Size(20, 20);
            this.tb_D4.TabIndex = 9;
            this.tb_D4.Enter += new System.EventHandler(this.tb_D4_Enter);
            // 
            // tb_D3
            // 
            this.tb_D3.Location = new System.Drawing.Point(300, 42);
            this.tb_D3.Mask = "&&";
            this.tb_D3.Name = "tb_D3";
            this.tb_D3.Size = new System.Drawing.Size(20, 20);
            this.tb_D3.TabIndex = 8;
            this.tb_D3.Enter += new System.EventHandler(this.tb_D3_Enter);
            // 
            // tb_D2
            // 
            this.tb_D2.Location = new System.Drawing.Point(255, 42);
            this.tb_D2.Mask = "&&";
            this.tb_D2.Name = "tb_D2";
            this.tb_D2.Size = new System.Drawing.Size(20, 20);
            this.tb_D2.TabIndex = 7;
            this.tb_D2.Enter += new System.EventHandler(this.tb_D2_Enter);
            // 
            // tb_D1
            // 
            this.tb_D1.Location = new System.Drawing.Point(210, 42);
            this.tb_D1.Mask = "&&";
            this.tb_D1.Name = "tb_D1";
            this.tb_D1.Size = new System.Drawing.Size(20, 20);
            this.tb_D1.TabIndex = 6;
            this.tb_D1.Enter += new System.EventHandler(this.tb_D1_Enter);
            // 
            // tb_D0
            // 
            this.tb_D0.Location = new System.Drawing.Point(165, 42);
            this.tb_D0.Mask = "&&";
            this.tb_D0.Name = "tb_D0";
            this.tb_D0.Size = new System.Drawing.Size(20, 20);
            this.tb_D0.TabIndex = 5;
            this.tb_D0.Enter += new System.EventHandler(this.tb_D0_Enter);
            // 
            // tb_DLC
            // 
            this.tb_DLC.Location = new System.Drawing.Point(76, 42);
            this.tb_DLC.Mask = "0";
            this.tb_DLC.Name = "tb_DLC";
            this.tb_DLC.Size = new System.Drawing.Size(20, 20);
            this.tb_DLC.TabIndex = 3;
            this.tb_DLC.Enter += new System.EventHandler(this.tb_DLC_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(477, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "D7";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(435, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "D6";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(387, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "D5";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(342, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "D4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(298, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "D3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(253, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "D2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(208, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "D1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(163, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "D0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(70, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "DLC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_Refresh,
            this.cmb_BaudRate,
            this.cmb_InterfaceList,
            this.btn_OnOff,
            this.toolStripSeparator2,
            this.btn_Clean,
            this.btn_Filter});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(870, 50);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Refresh.Image = global::LAWICEL_CANalyzer.Properties.Resources.refreshIcon;
            this.btn_Refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(51, 47);
            this.btn_Refresh.Text = "toolStripButton2";
            this.btn_Refresh.ToolTipText = "Refresh";
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // cmb_BaudRate
            // 
            this.cmb_BaudRate.BackColor = System.Drawing.SystemColors.Window;
            this.cmb_BaudRate.Items.AddRange(new object[] {
            "1000",
            "800",
            "500",
            "250",
            "125",
            "100",
            "50"});
            this.cmb_BaudRate.Name = "cmb_BaudRate";
            this.cmb_BaudRate.Size = new System.Drawing.Size(121, 50);
            this.cmb_BaudRate.ToolTipText = "Baud Rate [kHz]";
            // 
            // cmb_InterfaceList
            // 
            this.cmb_InterfaceList.BackColor = System.Drawing.Color.Salmon;
            this.cmb_InterfaceList.Name = "cmb_InterfaceList";
            this.cmb_InterfaceList.Size = new System.Drawing.Size(121, 50);
            this.cmb_InterfaceList.ToolTipText = "Interface Name";
            // 
            // btn_OnOff
            // 
            this.btn_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_OnOff.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_OnOff.Image = global::LAWICEL_CANalyzer.Properties.Resources.onIcon;
            this.btn_OnOff.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_OnOff.Name = "btn_OnOff";
            this.btn_OnOff.Size = new System.Drawing.Size(51, 47);
            this.btn_OnOff.Tag = "On";
            this.btn_OnOff.ToolTipText = "Connect";
            this.btn_OnOff.Click += new System.EventHandler(this.btn_OnOff_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_Clean
            // 
            this.btn_Clean.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Clean.Image = global::LAWICEL_CANalyzer.Properties.Resources.rubber;
            this.btn_Clean.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Clean.Name = "btn_Clean";
            this.btn_Clean.Size = new System.Drawing.Size(51, 47);
            this.btn_Clean.Text = "toolStripButton1";
            this.btn_Clean.ToolTipText = "Clean";
            this.btn_Clean.Click += new System.EventHandler(this.btn_Clean_Click);
            // 
            // btn_Filter
            // 
            this.btn_Filter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Filter.Image = global::LAWICEL_CANalyzer.Properties.Resources.filterIcon;
            this.btn_Filter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Filter.Name = "btn_Filter";
            this.btn_Filter.Size = new System.Drawing.Size(51, 47);
            this.btn_Filter.Text = "toolStripButton2";
            this.btn_Filter.ToolTipText = "Filter";
            this.btn_Filter.Click += new System.EventHandler(this.btn_Filter_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 689);
            this.Controls.Add(this.panel_CANTrace);
            this.Controls.Add(this.panel_CANUSB_Statistics);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "LAWICEL CANalyzer";
            this.panel_CANUSB_Statistics.ResumeLayout(false);
            this.panel_CANTrace.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANTrace)).EndInit();
            this.panel2.ResumeLayout(false);
            this.grp_Scripts.ResumeLayout(false);
            this.grp_SendMessage.ResumeLayout(false);
            this.grp_SendMessage.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel_CANUSB_Statistics;
        private System.Windows.Forms.Panel panel_CANTrace;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_CANTrace;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTR;
        private System.Windows.Forms.DataGridViewTextBoxColumn D0;
        private System.Windows.Forms.DataGridViewTextBoxColumn D1;
        private System.Windows.Forms.DataGridViewTextBoxColumn D2;
        private System.Windows.Forms.DataGridViewTextBoxColumn D3;
        private System.Windows.Forms.DataGridViewTextBoxColumn D4;
        private System.Windows.Forms.DataGridViewTextBoxColumn D5;
        private System.Windows.Forms.DataGridViewTextBoxColumn D6;
        private System.Windows.Forms.DataGridViewTextBoxColumn D7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Timestamp;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_Refresh;
        private System.Windows.Forms.ToolStripComboBox cmb_BaudRate;
        private System.Windows.Forms.ToolStripComboBox cmb_InterfaceList;
        private System.Windows.Forms.ToolStripButton btn_OnOff;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.GroupBox grp_SendMessage;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cb_RTR;
        private System.Windows.Forms.MaskedTextBox tb_D7;
        private System.Windows.Forms.MaskedTextBox tb_D6;
        private System.Windows.Forms.MaskedTextBox tb_D5;
        private System.Windows.Forms.MaskedTextBox tb_D4;
        private System.Windows.Forms.MaskedTextBox tb_D3;
        private System.Windows.Forms.MaskedTextBox tb_D2;
        private System.Windows.Forms.MaskedTextBox tb_D1;
        private System.Windows.Forms.MaskedTextBox tb_D0;
        private System.Windows.Forms.MaskedTextBox tb_DLC;
        private System.Windows.Forms.MaskedTextBox tb_ID;
        private System.Windows.Forms.GroupBox grp_Scripts;
        private System.Windows.Forms.Button btn_startScripts;
        private System.Windows.Forms.Button btn_selectScripts;
        private System.Windows.Forms.Button btn_createScript;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripButton btn_Clean;
        private System.Windows.Forms.ToolStripButton btn_Filter;
    }
}

