﻿namespace LAWICEL_CANalyzer
{
    partial class Form_Filter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Filter));
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Submit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dgv_passRange = new System.Windows.Forms.DataGridView();
            this.btn_Deselect = new System.Windows.Forms.Button();
            this.btn_Select = new System.Windows.Forms.Button();
            this.dgv_stopRange = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.feature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stopRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_passRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_stopRange)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.ImageKey = "cancel.png";
            this.btn_Cancel.Location = new System.Drawing.Point(284, 406);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_Cancel.TabIndex = 16;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            // 
            // btn_Submit
            // 
            this.btn_Submit.ImageKey = "ok.png";
            this.btn_Submit.Location = new System.Drawing.Point(142, 406);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(50, 50);
            this.btn_Submit.TabIndex = 15;
            this.btn_Submit.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 86);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter Conditions";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Pass",
            "Stop"});
            this.comboBox2.Location = new System.Drawing.Point(89, 37);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(108, 21);
            this.comboBox2.TabIndex = 17;
            // 
            // button1
            // 
            this.button1.ImageKey = "ok.png";
            this.button1.Location = new System.Drawing.Point(370, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 50);
            this.button1.TabIndex = 16;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(305, 55);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "HEX";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(305, 22);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(47, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "DEC";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(211, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(77, 20);
            this.textBox1.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "ID",
            "DLC",
            "D0",
            "D1",
            "D2",
            "D3",
            "D4",
            "D5",
            "D6",
            "D7"});
            this.comboBox1.Location = new System.Drawing.Point(20, 37);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(57, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // dgv_passRange
            // 
            this.dgv_passRange.AllowUserToAddRows = false;
            this.dgv_passRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_passRange.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.feature,
            this.passRange});
            this.dgv_passRange.Location = new System.Drawing.Point(12, 104);
            this.dgv_passRange.Name = "dgv_passRange";
            this.dgv_passRange.ReadOnly = true;
            this.dgv_passRange.RowHeadersVisible = false;
            this.dgv_passRange.Size = new System.Drawing.Size(180, 271);
            this.dgv_passRange.TabIndex = 18;
            // 
            // btn_Deselect
            // 
            this.btn_Deselect.ImageKey = "prevArrow.png";
            this.btn_Deselect.Location = new System.Drawing.Point(213, 254);
            this.btn_Deselect.Name = "btn_Deselect";
            this.btn_Deselect.Size = new System.Drawing.Size(50, 50);
            this.btn_Deselect.TabIndex = 21;
            this.btn_Deselect.UseVisualStyleBackColor = true;
            // 
            // btn_Select
            // 
            this.btn_Select.ImageKey = "nextArrow.png";
            this.btn_Select.Location = new System.Drawing.Point(213, 190);
            this.btn_Select.Name = "btn_Select";
            this.btn_Select.Size = new System.Drawing.Size(50, 50);
            this.btn_Select.TabIndex = 19;
            this.btn_Select.UseVisualStyleBackColor = true;
            // 
            // dgv_stopRange
            // 
            this.dgv_stopRange.AllowUserToAddRows = false;
            this.dgv_stopRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_stopRange.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.stopRange});
            this.dgv_stopRange.Location = new System.Drawing.Point(284, 104);
            this.dgv_stopRange.Name = "dgv_stopRange";
            this.dgv_stopRange.ReadOnly = true;
            this.dgv_stopRange.RowHeadersVisible = false;
            this.dgv_stopRange.Size = new System.Drawing.Size(172, 271);
            this.dgv_stopRange.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Feature";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Condition";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Value";
            // 
            // feature
            // 
            this.feature.HeaderText = "";
            this.feature.Name = "feature";
            this.feature.ReadOnly = true;
            this.feature.Width = 40;
            // 
            // passRange
            // 
            this.passRange.DataPropertyName = "Timestamp";
            this.passRange.HeaderText = "Pass";
            this.passRange.Name = "passRange";
            this.passRange.ReadOnly = true;
            this.passRange.Width = 120;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // stopRange
            // 
            this.stopRange.HeaderText = "Stop";
            this.stopRange.Name = "stopRange";
            this.stopRange.ReadOnly = true;
            this.stopRange.Width = 120;
            // 
            // Form_Filter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 473);
            this.Controls.Add(this.dgv_passRange);
            this.Controls.Add(this.btn_Deselect);
            this.Controls.Add(this.btn_Select);
            this.Controls.Add(this.dgv_stopRange);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Submit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Filter";
            this.Text = "Form_Filter";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_passRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_stopRange)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.DataGridView dgv_passRange;
        private System.Windows.Forms.Button btn_Deselect;
        private System.Windows.Forms.Button btn_Select;
        private System.Windows.Forms.DataGridView dgv_stopRange;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn feature;
        private System.Windows.Forms.DataGridViewTextBoxColumn passRange;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn stopRange;
    }
}