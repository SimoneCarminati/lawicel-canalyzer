﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace LAWICEL_CANalyzer
{
    class ThreadTestVita
    {
        private List<CANCommand> _comandi;
        private int counter;
        private int indexComando;
        private int scriptNumber;

        private bool running;

        public Thread thread;

        private CANusb_Connector _connector = null;

        public ThreadTestVita(CANusb_Connector connector, List<CANCommand> comandi, params int[] scriptNumber)
        {
            _connector = connector;
            this.scriptNumber = scriptNumber[0];
            this._comandi = comandi;
            reset();
        }

        public void reset()
        {
            counter = 0;
            indexComando = 0;
            running = false;
        }

        public void stopThread()
        {
            running = false;
        }

        public void startThread()
        {
            running = true;
            runTest();
        }

        public void runTest()
        {
            try
            {
                CANCommand command = null;
                while (running)
                {
                    command = _comandi[indexComando++];
                    if (indexComando >= _comandi.Count)
                    {
                        indexComando = 0;
                    }

                    _connector.enqueueMessage(command);
                    counter++;
                    if (command.TimeStamp > 0)
                    {
                        Thread.Sleep(Convert.ToInt32(command.TimeStamp));
                    }
                }
            }
            catch (ThreadInterruptedException ex)
            {
                ;// LogManager.GetLogger(_protocol.Connector.Logger.Name).Trace(_protocol.Connector.Logger.Name + " Thread " + this.scriptNumber + " Test halted");
            }
        }

    }
}
