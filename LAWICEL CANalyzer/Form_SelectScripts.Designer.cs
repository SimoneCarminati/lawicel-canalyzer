﻿namespace LAWICEL_CANalyzer
{
    partial class Form_SelectScripts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_SelectScripts));
            this.btn_Select = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btn_Deselect = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Submit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_scriptsNotSelected = new System.Windows.Forms.DataGridView();
            this.scriptsNotSelected = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv_scriptsSelected = new System.Windows.Forms.DataGridView();
            this.scriptsSelected = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_scriptsNotSelected)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_scriptsSelected)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Select
            // 
            this.btn_Select.ImageKey = "nextArrow.png";
            this.btn_Select.ImageList = this.imageList;
            this.btn_Select.Location = new System.Drawing.Point(373, 157);
            this.btn_Select.Name = "btn_Select";
            this.btn_Select.Size = new System.Drawing.Size(50, 50);
            this.btn_Select.TabIndex = 2;
            this.btn_Select.UseVisualStyleBackColor = true;
            this.btn_Select.Click += new System.EventHandler(this.btn_Select_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            // 
            // btn_Deselect
            // 
            this.btn_Deselect.ImageKey = "prevArrow.png";
            this.btn_Deselect.ImageList = this.imageList;
            this.btn_Deselect.Location = new System.Drawing.Point(373, 221);
            this.btn_Deselect.Name = "btn_Deselect";
            this.btn_Deselect.Size = new System.Drawing.Size(50, 50);
            this.btn_Deselect.TabIndex = 3;
            this.btn_Deselect.UseVisualStyleBackColor = true;
            this.btn_Deselect.Click += new System.EventHandler(this.btn_Deselect_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_Deselect);
            this.panel1.Controls.Add(this.btn_Select);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_Cancel);
            this.panel4.Controls.Add(this.btn_Submit);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 358);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 92);
            this.panel4.TabIndex = 0;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.ImageKey = "cancel.png";
            this.btn_Cancel.ImageList = this.imageList;
            this.btn_Cancel.Location = new System.Drawing.Point(408, 22);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_Cancel.TabIndex = 16;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Submit
            // 
            this.btn_Submit.ImageKey = "ok.png";
            this.btn_Submit.ImageList = this.imageList;
            this.btn_Submit.Location = new System.Drawing.Point(340, 22);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(50, 50);
            this.btn_Submit.TabIndex = 15;
            this.btn_Submit.UseVisualStyleBackColor = true;
            this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_scriptsNotSelected);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(320, 450);
            this.panel2.TabIndex = 5;
            // 
            // dgv_scriptsNotSelected
            // 
            this.dgv_scriptsNotSelected.AllowUserToAddRows = false;
            this.dgv_scriptsNotSelected.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_scriptsNotSelected.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.scriptsNotSelected});
            this.dgv_scriptsNotSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_scriptsNotSelected.Location = new System.Drawing.Point(0, 0);
            this.dgv_scriptsNotSelected.Name = "dgv_scriptsNotSelected";
            this.dgv_scriptsNotSelected.ReadOnly = true;
            this.dgv_scriptsNotSelected.Size = new System.Drawing.Size(320, 450);
            this.dgv_scriptsNotSelected.TabIndex = 2;
            // 
            // scriptsNotSelected
            // 
            this.scriptsNotSelected.HeaderText = "Scripts Not Selected";
            this.scriptsNotSelected.Name = "scriptsNotSelected";
            this.scriptsNotSelected.ReadOnly = true;
            this.scriptsNotSelected.Width = 260;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgv_scriptsSelected);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(480, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(320, 450);
            this.panel3.TabIndex = 6;
            // 
            // dgv_scriptsSelected
            // 
            this.dgv_scriptsSelected.AllowUserToAddRows = false;
            this.dgv_scriptsSelected.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_scriptsSelected.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.scriptsSelected});
            this.dgv_scriptsSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_scriptsSelected.Location = new System.Drawing.Point(0, 0);
            this.dgv_scriptsSelected.Name = "dgv_scriptsSelected";
            this.dgv_scriptsSelected.ReadOnly = true;
            this.dgv_scriptsSelected.Size = new System.Drawing.Size(320, 450);
            this.dgv_scriptsSelected.TabIndex = 2;
            // 
            // scriptsSelected
            // 
            this.scriptsSelected.DataPropertyName = "Timestamp";
            this.scriptsSelected.HeaderText = "Scripts Selected";
            this.scriptsSelected.Name = "scriptsSelected";
            this.scriptsSelected.ReadOnly = true;
            this.scriptsSelected.Width = 260;
            // 
            // Form_SelectScripts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_SelectScripts";
            this.Text = "Select Scripts";
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_scriptsNotSelected)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_scriptsSelected)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Select;
        private System.Windows.Forms.Button btn_Deselect;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.DataGridView dgv_scriptsNotSelected;
        private System.Windows.Forms.DataGridView dgv_scriptsSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn scriptsNotSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn scriptsSelected;
    }
}