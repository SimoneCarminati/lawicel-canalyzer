﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace LAWICEL_CANalyzer
{
    public class CANCommand : IEquatable<CANCommand>
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct SplitUlong
        {
            [FieldOffset(0)]
            public byte d0;
            [FieldOffset(1)]
            public byte d1;
            [FieldOffset(2)]
            public byte d2;
            [FieldOffset(3)]
            public byte d3;
            [FieldOffset(4)]
            public byte d4;
            [FieldOffset(5)]
            public byte d5;
            [FieldOffset(6)]
            public byte d6;
            [FieldOffset(7)]
            public byte d7;
            [FieldOffset(0)]
            public ulong data;
        }

        public enum MexType : int
        {
            Rx,Tx
        };

        private SplitUlong dataBytes;
        private int retCode=0;
        private uint id = 0x000;
        private byte flags = 0x00;
        private ulong dataULong;
        private byte dlc = 0;
        private byte[] data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        private uint timeStamp = 0;
        private int messageType = 0;//0 = Rx; 1 = Tx
        private int nextMessage = 0;
        private DateTime actDateTime;

        //costruttore di default
        public CANCommand() {}

        //overload costruttore 

        public CANCommand(uint id, byte dlc, byte flags, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7, uint timeStamp = 0, int nextMessage = 0)
        {
            this.retCode = 0;
            ID = id;
            Flags = flags;
            DLC = dlc;
            byte[] bytes = { d0, d1, d2, d3, d4, d5, d6, d7 };
            DataULong = BitConverter.ToUInt64(bytes, 0);
            TimeStamp = timeStamp;
        }

        //public uint ID { get { return this.id; } set { this.id = value; } }

        public int RetCode { get; set; }

        public uint ID { get; set; }

        public byte Flags { get; set; }

        public byte DLC { get; set; }

        public ulong DataULong { get { return this.dataULong; } set { this.dataULong = value; this.dataBytes.data = dataULong; DataBytes = dataBytes; } }

        public SplitUlong DataBytes { get; set; }

        public uint TimeStamp { get; set; }

        public int MessageType { get; set; }

        public DateTime ActDateTime { get; set; }

        public void cleanData()
        {
            if (DLC < 8)
                DataULong = DataULong & 0x00FFFFFFFFFFFFFF;
            if (DLC < 7)
                DataULong = DataULong & 0x0000FFFFFFFFFFFF;
            if (DLC < 6)
                DataULong = DataULong & 0x000000FFFFFFFFFF;
            if (DLC < 5)
                DataULong = DataULong & 0x00000000FFFFFFFF;
            if (DLC < 4)
                DataULong = DataULong & 0x0000000000FFFFFF;
            if (DLC < 3)
                DataULong = DataULong & 0x000000000000FFFF;
            if (DLC < 2)
                DataULong = DataULong & 0x00000000000000FF;
            if (DLC < 1)
                DataULong = DataULong & 0x0000000000000000;
        }

        public bool Equals(CANCommand cmd)
        {
            if (this.ID == cmd.ID && this.DLC == cmd.DLC && this.DataBytes.d0 == cmd.DataBytes.d0 && this.DataBytes.d1 == cmd.DataBytes.d1
                 && this.DataBytes.d2 == cmd.DataBytes.d2 && this.DataBytes.d3 == cmd.DataBytes.d3 && this.DataBytes.d4 == cmd.DataBytes.d4 && this.DataBytes.d5 == cmd.DataBytes.d5
                 && this.DataBytes.d6 == cmd.DataBytes.d6 && this.DataBytes.d7 == cmd.DataBytes.d7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public void setID(uint id)
        //{
        //    this.id = id;
        //}
        //public uint getID()
        //{
        //    return (this.id);
        //}
        //public void setFlags(byte flags)
        //{
        //    this.flags = flags;
        //}
        //public byte getFlags()
        //{
        //    return (this.flags);
        //}
        //public void setDLC(byte dlc)
        //{
        //    this.dlc = dlc;
        //}
        //public byte getDLC()
        //{
        //    return (this.dlc);
        //}
        //public void setTimeStamp(uint timeStamp)
        //{
        //    this.timeStamp = timeStamp;
        //}
        //public uint getTimeStamp()
        //{
        //    return (this.timeStamp);
        //}
        //public void setRetCode(int retCode)
        //{
        //    this.retCode = retCode;
        //}
        //public int getRetCode()
        //{
        //    return (this.retCode);
        //}
        //public void setIndexNextMsg(int idx_next)
        //{
        //    this.nextMessage = idx_next;
        //}
        //public int getIndexNextMsg()
        //{
        //    return (this.nextMessage);
        //}
        ///*METODI PER IL SETTING ED IL GETTING DEI DATI DEL MESSAGGIO (DO...D7), per ogni protocollo possibile (CANopen, CAN-CAL, CANBUS, CANSYSTEM)*/
        //public void setData(byte[] data)
        //{
        //    this.data = data;
        //    dato.data = 0;
        //    dato.d0 = this.data[0];
        //    dato.d1 = this.data[1];
        //    dato.d2 = this.data[2];
        //    dato.d3 = this.data[3];
        //    dato.d4 = this.data[4];
        //    dato.d5 = this.data[5];
        //    dato.d6 = this.data[6];
        //    dato.d7 = this.data[7];

        //}
        //public byte[] getData()
        //{
        //    return (this.data);
        //}
        //public void setData(ulong data)
        //{
        //    this.dato.data = data;
        //    this.data[0] = dato.d0;
        //    this.data[1] = dato.d1;
        //    this.data[2] = dato.d2;
        //    this.data[3] = dato.d3;
        //    this.data[4] = dato.d4;
        //    this.data[5] = dato.d5;
        //    this.data[6] = dato.d6;
        //    this.data[7] = dato.d7;
        //}
        //public ulong getDataUlong()
        //{
        //    return this.dato.data;
        //}
        //public byte getDatum(byte idxDatum)
        //{
        //    return (this.data[idxDatum]);
        //}
        //public void setDatum(byte idxDatum, byte datum)
        //{
        //    this.data[idxDatum] = datum;
        //}

        /* INPUT : value = valore intero da convertire;
         *         bytesNumber = numero di bytes dell'array restituito
         * OUTPUT : bytes = value convertito in byte*/
        //public byte[] getBytes(int value, int bytesNumber, int bytesOrder)
        //{
        //    byte[] bytes = new byte[bytesNumber];
        //    switch (bytesOrder)
        //    {
        //        case 0://big endian : MSB ... LSB
        //            for (int i = bytesNumber - 1; i >= 0; i--)
        //            {
        //                bytes[(bytesNumber - 1) - i] = Convert.ToByte(((ushort)(value >> (i * 8)) & 0xFF));
        //            }
        //            break;
        //        case 1://little endian : LSB ... MSB
        //            for (int i = 0 ; i <= bytesNumber - 1; i++)
        //            {
        //                bytes[i] = Convert.ToByte(((ushort)(value >> (i * 8)) & 0xFF));
        //            }
        //            break;
        //    }
        //    return bytes;
        //}

        /*DESCRIZIONE : Se DLC<8, mette a zero tutti i Data Bytes successivi
         *              ES : DLC = 6 -> D6 e D7 vengono inizializzati a zero
          INPUT : CANCommand : messaggio CAN da ripulire
          OUTPUT : None (il messaggio */
        //public void cleanData()
        //{
        //    if (this.getDLC() < 8)
        //    {
        //        for (int i = this.getDLC(); i < 8; i++)
        //        {
        //            this.setDatum((byte)i, 0);
        //        }
        //    }
        //}

        //public bool Equals(CANCommand cmd)
        //{
        //    if ( ID == cmd.ID /*this.getID() == cmd.getID()*/ && this.getDLC() == cmd.getDLC() && this.getData()[0] == cmd.getData()[0] && this.getData()[1] == cmd.getData()[1]
        //         && this.getData()[2] == cmd.getData()[2] && this.getData()[3] == cmd.getData()[3] && this.getData()[4] == cmd.getData()[4] && this.getData()[5] == cmd.getData()[5]
        //         && this.getData()[6] == cmd.getData()[6] && this.getData()[7] == cmd.getData()[7])
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
    }
}
