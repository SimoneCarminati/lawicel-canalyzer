﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAWICEL_CANalyzer
{
    public partial class Form_AddMessage : Form
    {
        public CANCommand msg;
        public Form_AddMessage()
        {
            InitializeComponent();
        }

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                msg = new CANCommand(Convert.ToUInt32(Utility.hexValidator(tb_ID.Text, 3), 16), Convert.ToByte(Utility.hexValidator(tb_DLC.Text, 1), 16), cb_RTR.Checked ? (byte)0x01 : (byte)0x00, Convert.ToByte(Utility.hexValidator(tb_D0.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D1.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D2.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D3.Text, 3), 16), Convert.ToByte(Utility.hexValidator(tb_D4.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D5.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D6.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D7.Text, 2), 16), Convert.ToUInt32(Utility.hexValidator(tb_Timestamp.Text, 7), 10));
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region focusMaskedtextBox

        private void tb_ID_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_ID.SelectAll();
        }

        private void tb_DLC_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_DLC.SelectAll();
        }

        private void tb_D0_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D0.SelectAll();
        }

        private void tb_D1_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D1.SelectAll();
        }

        private void tb_D2_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D2.SelectAll();
        }

        private void tb_D3_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D3.SelectAll();
        }

        private void tb_D4_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D4.SelectAll();
        }

        private void tb_D5_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D5.SelectAll();
        }

        private void tb_D6_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D6.SelectAll();
        }

        private void tb_D7_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D7.SelectAll();
        }

        private void tb_Timestamp_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_Timestamp.SelectAll();
        }

        private void SetMaskedTextBoxSelectAll(MaskedTextBox txtbox)
        {
            txtbox.SelectAll();
        }

        #endregion

    }
}
