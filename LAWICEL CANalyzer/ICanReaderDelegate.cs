﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAWICEL_CANalyzer
{
    interface ICanReaderDelegate<out CANCommand>
    {
        event Action<CANCommand> Handler;
    }
}
